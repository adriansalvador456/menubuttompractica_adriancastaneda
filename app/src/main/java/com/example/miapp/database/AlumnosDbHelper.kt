package com.appmenuButton.adrianapp.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class AlumnosDbHelper(context: Context): SQLiteOpenHelper(context,DATABASE_NAME,null,DATABASE_VERSION) {
    // Aqui se esta realizando un clase derivada, cuando tenga una clase derivada se tiene una superclase, esta manda el valor
    // en las clases derivadas
    // esta funcion es para la creacion de la tabla, union de clases, una clase dentro de otra clase
    companion object {
        private const val DATABASE_NAME = "sistemas.db"
        private const val DATABASE_VERSION = 2
    }

    init {
        val dbFile = context.getDatabasePath(DATABASE_NAME)
        println("Database path: ${dbFile.absolutePath}")
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("""
            CREATE TABLE ${DefinirTabla.Alumnos.TABLA} (
                ${DefinirTabla.Alumnos.ID} INTEGER PRIMARY KEY AUTOINCREMENT,
                ${DefinirTabla.Alumnos.MATRICULA} TEXT,
                ${DefinirTabla.Alumnos.NOMBRE} TEXT,
                ${DefinirTabla.Alumnos.DOMICILIO} TEXT,
                ${DefinirTabla.Alumnos.ESPECIALIDAD} TEXT,
                ${DefinirTabla.Alumnos.FOTO} TEXT
            )
        """)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS ${DefinirTabla.Alumnos.TABLA}")
        onCreate(db)
    }
}